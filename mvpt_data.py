import numpy as np
import pandas as pd
from pylab import plt, mpl
from scipy.optimize import minimize
plt.style.use('seaborn')
mpl.rcParams['savefig.dpi'] = 300
mpl.rcParams['font.family'] = 'serif'
np.set_printoptions(precision=5, suppress=True,
                    formatter={'float': lambda x: f'{x:6.3f}'})

import settings_mvpt_data as stg

class MarkovitzPorffolioData:
    
    def __init__(self):
        
        '''Methods and graphs to evaluate the best Mean-Variance portfolio versus data-driven input
        '''
        
        
    def dataframe(self):
        
        df = pd.read_csv('mvpt_data.csv', index_col=0)
        
        # Specifies the symbols (RICs) to be invested in
        symbols = ['AAPL.O', 'MSFT.O', 'INTC.O', 'AMZN.O', 'GLD']
        
        # Calculates the log returns for all time series
        rets = np.log(df[symbols] / df[symbols].shift(1)).dropna()
    
        return rets
    
    def plot(self):
        
        # Plots the normalized financial time series for the selected symbols
        #(df[symbols] / df[symbols].iloc[0]).plot(figsize=(12,6))
        (self.dataframe() / self.dataframe().iloc[0]).plot(figsize=(12,6))
    
    # Portfolio return
    def port_return(self, rets, weights):
    
        return np.dot(rets.mean(), weights)*252
    
    # Portfolio volatility
    def port_volatility(self, rets, weights):
    
        return np.dot(weights, np.dot(rets.cov()*252, weights))**0.5
    
    # Portfolio sharpe ratio (withn zero short rate)
    def port_sharpe(self, rets, weights):
    
        return self.port_return(rets, weights) / self.port_volatility(rets, weights)
    
    
    # Simulate portfolio weights adding up to 100%
    def monte_carlo_simulation(self, rets):
        
        symbols = ['AAPL.O', 'MSFT.O', 'INTC.O', 'AMZN.O', 'GLD']
    
        w = np.random.random((1000, len(symbols)))
        w = (w.T /w.sum(axis=1)).T
    
        # Derives the resulting portfolio volatilities and returns
        pvr = []
        for weights in w:
            nw = self.port_volatility(self.dataframe(), weights), self.port_return(self.dataframe(), weights)
            pvr.append(nw)
            
        pvr = np.array(pvr)
        
        # Calculates the resulting Sharpe ratio
        psr = pvr[:, 1] / pvr[:, 0]
    
    
        # Simulated portfolio volatilities, returns and Sharpe ratio
        plt.figure(figsize=(12,6))
        fig = plt.scatter(pvr[:, 0], pvr[:, 1],
                      c=psr, cmap='coolwarm')
        cb = plt.colorbar(fig)
        cb.set_label('Sharpe ratio')
        plt.xlabel('expected volatility')
        plt.ylabel('expected return')
        plt.title('  |  '.join(symbols));
        
    def expected_vs_realized_statistics(self):
        
        # select the relevant dataset for the given year
        # derives the porfolio weights that maximize the sharpe ratio
        # stores these weights in a dict object
        opt_weights = {}
        for year in range(2010, 2019):
            rets_ = self.dataframe().loc[f'{year}-01-01':f'{year}-12-31']
            ow = minimize(lambda weights: -self.port_sharpe(rets_, weights),
                          np.repeat(1/len(stg.symbols), len(stg.symbols)),
                          bounds=stg.bnds,
                          constraints=stg.cons)['x']
            opt_weights[year] = ow
            
        res = pd.DataFrame()
        for year in range(2010, 2019):
            rets_ = self.dataframe().loc[f'{year}-01-01':f'{year}-12-31']
            epv = self.port_volatility(rets_, opt_weights[year])
            epr = self.port_return(rets_, opt_weights[year])
            esr = epr/epv
            rets_ = self.dataframe().loc[f'{year + 1}-01-01':f'{year + 1}-12-31']
            rpv = self.port_volatility(rets_, opt_weights[year])
            rpr = self.port_return(rets_, opt_weights[year])
            rsr = rpr/rpv
            res = res.append(pd.DataFrame({'epv': epv, 'epr': epr, 'esr': esr,
                                           'rpv': rpv, 'rpr': rpr, 'rsr': rsr},
                                          index=[year + 1]))
            
        return res
    
    
    def EvRpv(self):
        
        plot = self.expected_vs_realized_statistics()[['epv', 'rpv']].plot(kind='bar', figsize=(12,6),
                                 title='Expected vs. Realized Portfolio Volatility');
        
        return plot

    def EvRpr(self):
        
        plot = self.expected_vs_realized_statistics()[['epr', 'rpr']].plot(kind='bar', figsize=(12,6),
                                 title='Expected vs. Realized Portfolio Return');
        
        return plot
    

    def EvRps(self):
        
        plot = self.expected_vs_realized_statistics()[['esr', 'rsr']].plot(kind='bar', figsize=(12,6),
                         title='Expected vs. Realized Portfolio Sharpe Ratio');
        
        return plot
        
        
        
        
        
        
        
    

